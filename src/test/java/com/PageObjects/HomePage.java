package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Utilities.BrowserUtility;

public class HomePage extends BrowserUtility {
	public HomePage(WebDriver wd, WebDriverWait wait) {
		super(wd, wait);
		this.driver = driver;
		this.wait = wait;
	}
	private WebDriver driver;
	private WebDriverWait wait;

}
