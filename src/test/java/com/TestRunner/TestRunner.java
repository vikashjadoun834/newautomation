package com.TestRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)


@CucumberOptions(features=".//Features/Login.feature"
,glue="com.StepDefinition",
monochrome=true
,plugin = { "pretty", "html:target/Reports/reports.html"}

)



public class TestRunner {

}
