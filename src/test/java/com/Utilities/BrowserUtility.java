package com.Utilities;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserUtility {

	private WebDriver wd;
	private WebDriverWait wait;

	public BrowserUtility(WebDriver wd, WebDriverWait wait) {
		super();
		this.wd = wd;
		this.wait = wait;
	}

	public void enterTextById(String elementLocator, String textToEnter) {
		By elementLocatorBy = By.id(elementLocator);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocatorBy));
		} catch (TimeoutException e) {
			System.err.print("Unable to perform the action because" + e);
			System.out.println("Locator Used" + elementLocator);
		}
		element.sendKeys(textToEnter);
	}

	public void enterTextBy(By elementLocator, String textToEnter) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
		element.sendKeys(textToEnter);
	}

	public void enterTextBy(By elementLocator, Keys textToEnter) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
		element.sendKeys(textToEnter);
	}

	public void enterTextByName(String elementLocator, String textToEnter) {
		By elementLocatorBy = By.name(elementLocator);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocatorBy));
		element.sendKeys(textToEnter);
	}

	public void clickOn(By elementLocator) {
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
		element.click();
	}

	public void selectFromDropDown(By dropDownLocator, int index) {
		WebElement elementLocator = wd.findElement(dropDownLocator);
		Select element = new Select(elementLocator);
		element.selectByIndex(index);

	}

	public void selectFromDropDown(By dropDownLocator, String visibleText) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(dropDownLocator));
		Select element = new Select(elementLocator);
		element.selectByVisibleText(visibleText);

	}

	public void switchToFrame(String idOrName) {
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(idOrName));
	}

	public String readAttribute(By locator, String attributeName) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return elementLocator.getAttribute(attributeName);

	}

	public String readAttribute(WebElement element, String attributeName) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOf(element));

		return elementLocator.getAttribute(attributeName);

	}

	public int returnTotalElementCount(By elementLocator) {
		List<WebElement> elementList = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementLocator));
		return elementList.size();

	}

	public List<String> verifyLinkBroken(By elementLocator) {
		List<WebElement> elementList = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementLocator));
		List<String> urlList = new ArrayList<String>();
		for (WebElement element : elementList) {
			urlList.add(readAttribute(element, "href"));
		}
		return urlList;
	}

	public void clickOnElementFromTheList(By elementListLocator, int index) {
		List<WebElement> elementList = wait
				.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementListLocator));
		WebElement element = elementList.get(index);
		element.click();
	}

	public boolean isElementPresent(By elementLocator) {
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));

		}

		catch (TimeoutException e) {
			System.out.println("Couldnot find the element with the locator" + elementLocator);
		}
		if (element == null) {
			return false;
		} else {
			return true;
		}
	}

	public void scrollBy(int pixelPoint) {
		// window.scrollBy()
		// JavaSriptExecutor ------> inject FrondEnd JavaScript into the DOM
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) wd;
		javascriptExecutor.executeScript("window.scrollBy(0," + pixelPoint + ")", "");
	}

}

